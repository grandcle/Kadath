/*
    Copyright 2017 Philippe Grandclement

    This file is part of Kadath.

    Kadath is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Kadath is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Kadath.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __BASE_TENSOR_HPP_
#define __BASE_TENSOR_HPP_



// Headers C
#include "headcpp.hpp"
#include "space.hpp"

// Types of basis :
#define CARTESIAN_BASIS 0
#define SPHERICAL_BASIS 1
#define MTZ_BASIS 2

namespace Kadath {

		    //-----------------------------------//
		    //	    class Base_tensor            //
		    //-----------------------------------//

/**
 * Describes the tensorial basis used by the various tensors. 
 * In each \c Domain it stores an integer describing the basis.
 * Possible choices :
 * \li CARTESIAN_BASIS : Cartesian 
 * \li SPHERICAL_BASIS : orthonormal spherical.
 * \li MTZ_BASIS : orthonormal basis, assuming the constant radius sections have a negative curvature.
 * \ingroup fields
 */
class Base_tensor : public Memory_mapped {
    
    // Data : 
    // -----
protected:
	const Space& space ; ///< The associated \c Space
	Array<int> basis ; ///< The basis in each \c Domain.


    // Constructors - Destructor
    // -------------------------
	
public:
	/**
	* Constructor, does not affect anything.
        * @param  sp : the \c Space.
	**/
	explicit Base_tensor(const Space& sp) : space{sp}, basis{sp.get_nbr_domains()} {basis = -1 ;};
	/**
	* Constructor where the basis is the same everywhere.
        * @param  sp : the \c Space.
	* @param bb : input basis.
	**/
	Base_tensor (const Space& sp, int bb)  : space{sp}, basis{sp.get_nbr_domains()} {basis = bb ;}
	/// Copy constructor
	Base_tensor(const Base_tensor& so) : space{so.space}, basis{so.basis} {}
	/**
	* Constructor from a file.
	* @param sp : the \c Space.
	* @param fd : the file (generated by save.
	*/
	Base_tensor (const Space& sp, FILE* fd) : space{sp}, basis{fd} {}

#ifdef TENSOR_MOVE_SEMANTIC
    Base_tensor(Base_tensor && so) : space{so.space}, basis{std::move(so.basis)} {}
    Base_tensor & operator=(Base_tensor && so) {assert(&space == &so.space); basis =  std::move(so.basis); return *this;}
#endif
	virtual ~Base_tensor() = default;			///< Destructor

public:
	
	/// Read/write the basis in a given domain
	int& set_basis(int nd) {return basis.set(nd) ;}
	/// Read only the basis in a given domain
	int get_basis (int nd) const {return basis(nd) ;} 
	/// Affectation operator
	void operator= (const Base_tensor&) ;
	//! Sylvain's stuff
	void swap(Base_tensor & so) {assert(&space == &so.space); basis.swap(so.basis);}

	/**
	* @returns : the \c Space (read only)/
	*/
	const Space& get_space () const {return space;} ; 
	
	void save (FILE*fd) const {basis.save(fd) ;} ///< Saving function

    // Outputs
    // ------- 
    friend ostream& operator<<(ostream& , const Base_tensor& ) ; ///< Display
    friend bool operator== (const Base_tensor&, const Base_tensor&) ; ///< Tests equality of two basis.
    friend bool operator!= (const Base_tensor&, const Base_tensor&) ; ///< Tests the difference of two basis.
};

inline void Base_tensor::operator= (const Base_tensor& so) {
    assert (&space==&so.get_space()) ;
    for (int i=0 ; i<basis.get_size(0) ; i++)
	basis.set(i) = so.basis(i) ;
}


inline bool operator!= (const Base_tensor& b1, const Base_tensor& b2) {
    return !(b1 == b2);
}

}
#endif
