/*
    Copyright 2020 sauliac

    This file is part of Kadath.

    Kadath is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Kadath is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Kadath.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "config.h"
#ifdef PAR_VERSION
#include "mpi.h"
#endif
#include "system_of_eqs.hpp"
#include "matrice.hpp"
#include "scalar.hpp"
#include "tensor_impl.hpp"
#include "array_math.hpp"

namespace Kadath {

    template<>
    bool System_of_eqs::do_newton<Computational_model::mpi_parallel>(double precision, double &error) {
        auto & os = *output_stream;
        bool res;
#ifdef PAR_VERSION
        niter++;

        // rank and nproc from MPI :
        int rank, nproc,mpi_world_size;
        MPI_Comm_size (MPI_COMM_WORLD, &mpi_world_size);
        MPI_Comm_rank (MPI_COMM_WORLD, &rank);
        MPI_Comm_size (MPI_COMM_WORLD, &nproc);

//        if(rank==0 && niter==1 && verbosity)
//        {
//            display_do_newton_report_header(os,precision);
//        }
        //vars_to_terms();
        Array<double> second (sec_member());
        error = max(fabs(second));
        if (rank==0) 
        	cout << "Entering do_newton with error " << error << endl ;
        int second_member_size = second.get_size(0);
        int bsize{static_cast<int>(default_block_size)};
	/*int bsize{};
        {
            double const dsms{static_cast<double>(second_member_size)}, dncpn{static_cast<double>(nb_core_per_node)};
            int bsexp {static_cast<int>(std::floor(std::log2(dsms/dncpn)))};
            bsexp = std::max(0,bsexp);
            bsize = std::min(static_cast<int>(default_block_size),static_cast<int>(std::pow(2,bsexp)));
        }*/

        if (error<precision) {
            res = true;
//            if(rank==0 && verbosity)
//            {
//                display_do_newton_ending_line(os,precision,error);
//                os  << endl;
//            }
        }
        else {
            if (second_member_size != nbr_unknowns) {
                cerr << "Number of unknowns is different from the number of equations" << endl;
                cerr << "nbr equations = " << second_member_size << endl;
                cerr << "nbr unknowns  = " << nbr_unknowns << endl;
                abort();
            }

            // Computation in a 1D distributed matrice
            int zero_i {0}; double zero_d {0.};
            int one_i {1}; double one_d {1.};
            int ictxt_in;
            int nprow_in = 1;
            int npcol_in = nproc;
            sl_init_ (&ictxt_in, &nprow_in, &npcol_in);

            // Get local processor row and mycol
            int proc_row_in, proc_col_in;
            blacs_gridinfo_ (&ictxt_in, &nprow_in, &npcol_in, &proc_row_in, &proc_col_in);

            while (bsize*nproc > second_member_size) bsize /= 2;
            if (bsize<1) {
                cerr << "Too many processors in do_newton" << endl;
                abort();
            }
            int nblock_per_proc {(second_member_size / bsize) / nproc};
            int remain_block {(second_member_size / bsize) % nproc};
            //adjust block size to avoid idle process when the remaining workload is still significant
            /*int best_bsize {bsize};
            int best_remain_workload {remain_block};
            while((remain_block <= (nproc/2) && nblock_per_proc<12) && bsize > 0)
            {
                bsize = div(bsize,2).quot;
                if(bsize != 0) {
                    remain_block = (second_member_size / bsize) % nproc;
                    nblock_per_proc = (second_member_size / bsize) / nproc;
                    if (remain_block > best_remain_workload) {
                        best_remain_workload = remain_block;
                        best_bsize = bsize;
                    }
                }
            }
            //if no suitable block size has been found, the best found is chosen.
            if(bsize<1) bsize = best_bsize;*/
	    //if(rank==0 && default_block_size != bsize) std::cout << "block size adjusted down to " << bsize << std::endl;
            int nrow_in = numroc_ (&second_member_size, &bsize, &proc_row_in, &zero_i, &nprow_in);
            int ncol_in = numroc_ (&second_member_size, &bsize, &proc_col_in, &zero_i, &npcol_in);

//            Array<double> matloc_in (ncolloc_in, nrowloc_in);
            Array<double> matloc_in (nrow_in, ncol_in);
            int start = bsize*rank;

            Hash_key chrono_key = this->start_chrono("mpi_do_newton  problem_size ",
                                                     second_member_size, "  matrix_computation");
            try {
                compute_matrix_cyclic(matloc_in, second_member_size, start, bsize, nproc, DO_NOT_TRANSPOSE);
            }
            catch(std::exception const & e) {
                std::cerr << "Unable to compute the jacobian (size = " << second_member_size << "). \n";
                std::cerr << "the computational routines raised the following exception : " << e.what() << std::endl;
                abort();
            }
            // Descriptor of the matrix :
            Array<int> descamat_in(9);
            int info;
            descinit_ (descamat_in.set_data(), &second_member_size, &second_member_size, &bsize, &bsize, &zero_i, &zero_i, &ictxt_in, &nrow_in, &info);

            // Wait for everybody
            MPI_Barrier(MPI_COMM_WORLD);

            Duration const t_load_matrix {this->stop_chrono(chrono_key)};

            chrono_key = this->start_chrono("mpi_do_newton  problem_size ",
                                            second_member_size, "  matrix_translation");
            // Now translate to a 2D cyclic decomposition
            int npcol, nprow;
            split_two_d (nproc, npcol, nprow);
            int ictxt;
            sl_init_ (&ictxt, &nprow, &npcol);

            int myrow, mycol;
            blacs_gridinfo_ (&ictxt, &nprow, &npcol, &myrow, &mycol);

            int nrowloc = numroc_ (&second_member_size, &bsize, &myrow, &zero_i, &nprow);
            int ncolloc = numroc_ (&second_member_size, &bsize, &mycol, &zero_i, &npcol);

            Array<double> matloc (ncolloc, nrowloc);
            Array<int> descamat(9);
            descinit_ (descamat.set_data(), &second_member_size, &second_member_size, &bsize, &bsize, &zero_i, &zero_i, &ictxt, &nrowloc, &info);

            Cpdgemr2d (second_member_size, second_member_size, matloc_in.set_data(), 1, 1, descamat_in.set_data(), matloc.set_data(), 1, 1, descamat.set_data(), ictxt);

            matloc_in.delete_data();

            // Translate the second member :
            Array<double> secloc (nrowloc);
            for (int row=0 ; row < second_member_size ; row++) {
                int pi = div(row/bsize, nprow).rem;
                int li = int(row/(nprow*bsize));
                int xi = div(row, bsize).rem;
                if ((pi==myrow) && (mycol==0))
                    secloc.set(li*bsize+xi) = second(row);
            }

            // Descriptor of the second member
            Array<int> descsec(9);
            int one = 1;
            descinit_ (descsec.set_data(), &second_member_size, &one, &bsize, &bsize, &zero_i, &zero_i, &ictxt, &nrowloc, &info);
            Duration const t_trans_matrix {this->stop_chrono(chrono_key)};

            // Inversion
            Array<int> ipiv (second_member_size);
            chrono_key = this->start_chrono("mpi_do_newton  problem_size ",
                                            second_member_size, "  matrix_inversion ");
            pdgesv_ (&second_member_size, &one, matloc.set_data(), &one, &one, descamat.set_data(), ipiv.set_data(), secloc.set_data(), &one, &one, descsec.set_data(), &info);
            Duration const t_inv_matrix {this->stop_chrono(chrono_key)};

            chrono_key = this->start_chrono("mpi_do_newton  problem_size ", second_member_size, "  newton_update ");
            // Get the global solution
            Array<double> auxi(second_member_size);
            auxi = 0.;
            for (int row=0 ; row < second_member_size ; row++) {
                int pi = div(row/bsize, nprow).rem;
                int li = int(row/(nprow*bsize));
                int xi = div(row, bsize).rem;
                if ((pi==myrow) && (mycol==0))
                    auxi.set(row) = secloc(li*bsize+xi);
            }

            Array<double> xx (second_member_size);
            MPI_Allreduce (auxi.set_data(), xx.set_data(), second_member_size, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

            blacs_gridexit_ (&ictxt);
            blacs_gridexit_ (&ictxt_in);

            newton_update_vars(xx);

            Duration const t_newton_update {this->stop_chrono(chrono_key)};
            current_output_data = Output_data{niter, second_member_size, error, t_load_matrix, t_trans_matrix,
                                              t_inv_matrix, t_newton_update};
//            if (rank == 0 && verbosity) {
//                display_do_newton_iteration(os,
//                        {niter, second_member_size, error, t_load_matrix, t_trans_matrix, t_inv_matrix, t_newton_update});
//            }
            res = false;
        }
#else
        cerr << "Error : cannot call System_of_eqs::do_newton<mpi_parallel> without MPI. " << endl;
#endif
        return res;
    }


    // Definitions for the linesearch-related methods.

    template<>
    void System_of_eqs::check_positive<Computational_model::mpi_parallel>(double delta)
    {
        if (delta < 0.0)
        {
            cerr << "roundoff error in do_newton_with_linesearch" << endl;
            abort();
        }
    }

    template<>
    void System_of_eqs::check_negative<Computational_model::mpi_parallel>(double delta)
    {
        if (delta >= 0.0)
        {
            cerr << "roundoff error in do_newton_with_linesearch" << endl;
            abort();
        }
    }

    template<>
    double System_of_eqs::compute_f<Computational_model::mpi_parallel>(Array<double> const& second)
    {
        double f(0.0);
        for (int i(0) ; i < second.get_nbr() ; ++i) f += pow(second.get_data()[i],2) ;
        return f;
    }

    template<>
    void System_of_eqs::check_size_VS_unknowns<Computational_model::mpi_parallel>(int n)
    {
        if (n != nbr_unknowns)
        {
            cerr << "Number of unknowns is different from the number of equations" << endl;
            cerr << "nbr equations = " << n << endl;
            cerr << "nbr unknowns  = " << nbr_unknowns << endl;
            abort();
        }
    }

    template<>
    void System_of_eqs::check_bsize<Computational_model::mpi_parallel>(int bsize)
    {
        if (bsize < 1)
        {
            cerr << "Too many processrs in do_newton" << endl;
            abort();
        }
    }

    template<>
    void System_of_eqs::compute_matloc<Computational_model::mpi_parallel>(Array<double>& matloc_in, int nn, int bsize)
    {
#ifdef PAR_VERSION
        int rank, nproc;
        MPI_Comm_rank (MPI_COMM_WORLD, &rank);
        MPI_Comm_size (MPI_COMM_WORLD, &nproc);

        int start(bsize*rank);
        bool done(false);
        int current(0);
        while (!done)
        {
            for (int i(0) ; i < bsize ; ++i)
                if (start+i < nn)
                {
                    Array<double> column(do_col_J(start+i));
                    for (int j(0) ; j < nn ; ++j) matloc_in.set(current,j) = column(j);
                    current++;
                }
            start += nproc*bsize;
            if (start >= nn) done = true;
        }
#else
        cerr << "Error : cannot call System_of_eqs::compute_matloc<mpi_parallel> without MPI." << endl;
#endif
    }

    template<>
    void System_of_eqs::translate_second_member<Computational_model::mpi_parallel>(Array<double>& secloc, Array<double> const& second, int nn, int bsize, int nprow, int myrow, int mycol)
    {
        for (int row(0) ; row < nn ; ++row)
        {
            int pi(div(row/bsize, nprow).rem);
            int li(int(row/(nprow*bsize)));
            int xi(div(row, bsize).rem);
            if ((pi == myrow) and (mycol == 0)) secloc.set(li*bsize+xi) = second(row);
        }
    }

    template<>
    void System_of_eqs::get_global_solution<Computational_model::mpi_parallel>(Array<double>& auxi, Array<double> const& secloc, int nn, int bsize, int nprow, int myrow, int mycol)
    {
        auxi = 0.0;
        for (int row(0) ; row < nn ; ++row)
        {
            int pi(div(row/bsize, nprow).rem);
            int li(int(row/(nprow*bsize)));
            int xi(div(row, bsize).rem);
            if ((pi == myrow) and (mycol == 0)) auxi.set(row) = secloc(li*bsize+xi);
        }
    }

    template<>
    void System_of_eqs::update_fields<Computational_model::mpi_parallel>(double lambda, vector<double> const& old_var_double, vector<Tensor> const& old_var_fields, vector<double> const& p_var_double, vector<Tensor> const& p_var_fields)
    {
#ifdef PAR_VERSION
        if (nvar_double > 0) for (int i(0) ; i < nvar_double ; ++i) *var_double[i] = old_var_double[i] - lambda*p_var_double[i];
        for (int i(0) ; i < nvar ; ++i) *var[i] = old_var_fields[i] - lambda*p_var_fields[i];
#else
        cerr << "Error : cannot call System_of_eqs::update_fields<mpi_parallel> without MPI." << endl;
#endif
    }

    template<>
    void System_of_eqs::compute_old_and_var<Computational_model::mpi_parallel>(Array<double> const& xx, vector<double>& old_var_double, vector<Tensor>& old_var_fields, vector<double>& p_var_double, vector<Tensor>& p_var_fields)
    {
        auto & os = *output_stream;
#ifdef PAR_VERSION
        int rank;
        MPI_Comm_rank (MPI_COMM_WORLD, &rank);
        int conte(0);
        espace.xx_to_vars_variable_domains(this, xx, conte);
        if (nvar_double > 0) for (int i(0) ; i < nvar_double ; ++i) old_var_double.push_back(*var_double[i]);
        for (int i(0) ; i < nvar ; ++i) old_var_fields.push_back(Tensor(*var[i]));
        xx_to_vars(xx, conte);
        if (nvar_double > 0) for (int i(0) ; i < nvar_double ; ++i) p_var_double.push_back(*var_double[i]);
        for (int i(0) ; i < nvar ; ++i) p_var_fields.push_back(Tensor(*var[i]));
#else
        cerr << "Error : cannot call System_of_eqs::compute_old_var<mpi_parallel> without MPI." << endl;
#endif
    }

    template<>
    void System_of_eqs::compute_p<Computational_model::mpi_parallel>(Array<double>& xx, Array<double> const& second, int nn)
    {
        auto & os = *output_stream;
#ifdef PAR_VERSION
        int bsize(64);
        // rank and nproc from MPI :
        int rank, nproc;
        MPI_Comm_rank (MPI_COMM_WORLD, &rank);
        MPI_Comm_size (MPI_COMM_WORLD, &nproc);

        // Computation in a 1D distributed matrice
        int zero(0);
        int ictxt_in;
        int nprow_in(1);
        int npcol_in(nproc);
        sl_init_(&ictxt_in, &nprow_in, &npcol_in);

        // Get my row and mycol
        int myrow_in, mycol_in;
        blacs_gridinfo_ (&ictxt_in, &nprow_in, &npcol_in, &myrow_in, &mycol_in);
        while (bsize*nproc > nn) bsize = div(bsize,2).quot;
        check_bsize(bsize);
        int nrowloc_in(numroc_(&nn, &bsize, &myrow_in, &zero, &nprow_in));
        int ncolloc_in(numroc_(&nn, &bsize, &mycol_in, &zero, &npcol_in));
        Array<double> matloc_in(ncolloc_in, nrowloc_in);
        compute_matloc(matloc_in, nn, bsize);

        // Descriptor of the matrix :
        Array<int> descamat_in(9);
        int info;
        descinit_(descamat_in.set_data(), &nn, &nn, &bsize, &bsize, &zero, &zero, &ictxt_in, &nrowloc_in, &info);

        // Wait for everybody
        MPI_Barrier(MPI_COMM_WORLD);

        // Now translate to a 2D cyclic decomposition
        int npcol, nprow;
        int ictxt;
        int myrow, mycol;
        split_two_d(nproc, npcol, nprow);
        sl_init_(&ictxt, &nprow, &npcol);
        blacs_gridinfo_(&ictxt, &nprow, &npcol, &myrow, &mycol);
        int nrowloc(numroc_(&nn, &bsize, &myrow, &zero, &nprow));
        int ncolloc(numroc_(&nn, &bsize, &mycol, &zero, &npcol));
        Array<double> matloc(ncolloc, nrowloc);
        Array<int> descamat(9);
        descinit_(descamat.set_data(), &nn, &nn, &bsize, &bsize, &zero, &zero, &ictxt, &nrowloc, &info);
        Cpdgemr2d(nn, nn, matloc_in.set_data(), 1, 1, descamat_in.set_data(), matloc.set_data(), 1, 1, descamat.set_data(), ictxt);
        matloc_in.delete_data();

        // Translate the second member :
        Array<double> secloc(nrowloc);
        translate_second_member(secloc, second, nn, bsize, nprow, myrow, mycol);

        // Descriptor of the second member
        Array<int> descsec(9);
        int one(1);
        descinit_(descsec.set_data(), &nn, &one, &bsize, &bsize, &zero, &zero, &ictxt, &nrowloc, &info);

        // Inversion
        Array<int> ipiv(nn);
        pdgesv_(&nn, &one, matloc.set_data(), &one, &one, descamat.set_data(), ipiv.set_data(), secloc.set_data(), &one, &one, descsec.set_data(), &info);

        // Get the global solution
        Array<double> auxi(nn);
        get_global_solution(auxi, secloc, nn, bsize, nprow, myrow, mycol);
        MPI_Allreduce(auxi.set_data(), xx.set_data(), nn, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        blacs_gridexit_(&ictxt);
        blacs_gridexit_(&ictxt_in);
#else
        cerr << "Error : cannot call System_of_eqs::compute_p<mpi_parallel> without MPI." << endl;
#endif
    }

    template<>
    bool System_of_eqs::do_newton_with_linesearch<Computational_model::mpi_parallel>(double precision, double& error,
                                                                         int ntrymax, double stepmax)
    {
        auto & os = *output_stream;
#ifdef PAR_VERSION
        // Numerical recipes 2007, section 9.7.1
        bool res(false);
        int rank;
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);

        Array<double> second(sec_member());  // rhs of J.x = F
        error = max(fabs(second));
        if (rank == 0) os << "Error init = " << error << endl;
        if (error < precision) res = true;
        else
        {
            int nn(second.get_size(0));
            check_size_VS_unknowns(nn);
            if (rank == 0) os << "Size of the system " << nn << endl;

            Array<double> p(nn);             // solution of J.x = F
            double fold, f, slope, f2(0.0);
            double lambda(1.0), lambdatmp(0.0), lambda2(0.0);
            const double lambdamin(1.0e-10);
            const double alpha(1.0e-4);

            fold = compute_f(second);    // old value of f = 0.5*F.F
            slope = -2.0*fold;
            check_negative(slope);
            compute_p<Computational_model::mpi_parallel>(p, second, nn);    // all the parallel inversion of the jacobian is crammed into this function
            double pmax(max(p));
            if (rank == 0) os << "max(newton step) = " << pmax << endl;

            if (pmax > stepmax) p *= stepmax/pmax;      // rescale in case p is too large
            vector<double> old_var_double, p_var_double;
            vector<Tensor> old_var_fields, p_var_fields;
            compute_old_and_var(p, old_var_double, old_var_fields, p_var_double, p_var_fields);
            for (int itry(1) ; itry <= ntrymax ; ++itry)
            {
                update_fields(lambda, old_var_double, old_var_fields, p_var_double, p_var_fields);    // var now updated
                second = sec_member();
                f = compute_f(second);
                if (f <= 5.0e-13) break; // in double precision, linesearch in spoiled by numerical error as soon as the error is close to 1.0e-7 (f \propto error^2)
                if (f <= fold + alpha*lambda*slope or lambda <= lambdamin) break;
                if (rank == 0) os << "line search " << itry << " ====> ";
                if (lambda == 1.0) lambdatmp = -0.5*slope/(f - fold - slope);
                else
                {
                    double rhs1(f - fold - lambda*slope);
                    double rhs2(f2 - fold - lambda2*slope);
                    double a((rhs1/pow(lambda,2) - rhs2/pow(lambda2,2))/(lambda-lambda2));
                    double b((-lambda2*rhs1/pow(lambda,2) + lambda*rhs2/pow(lambda2,2))/(lambda-lambda2));
                    if (a == 0.0) lambdatmp = -0.5*slope/b;
                    double delta(pow(b,2) - 3.0*a*slope);
                    check_positive<Computational_model::mpi_parallel>(delta);
                    lambdatmp = (-b + sqrt(delta))/(3.0*a);
                    if (lambdatmp > 0.5*lambda) lambdatmp = 0.5*lambda;
                }
                lambda2 = lambda;
                f2 = f;
                lambda = max(lambdatmp, 0.1*lambda);
                if (rank == 0) os << "lambda = " << lambda << endl;
                if (itry == ntrymax) update_fields(lambda, old_var_double, old_var_fields, p_var_double, p_var_fields);
            }
        }
        return res;
#else
        cerr << "Error : cannot call System_of_eqs::do_newton_with_linesearch<mpi_parallel> without MPI." << endl;
        return false;
#endif
    }
}
